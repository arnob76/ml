import numpy as np;
import matplotlib.pyplot as pyplt;
from scipy.stats import multivariate_normal
import pylab as plab
from matplotlib.patches import Ellipse
from numpy import linalg as LA


def plot(points, mu, sigma,clusters):

    plab.plot(points[:,0], points[:,1], 'x')
    for i in range(clusters):
        draw_mean_cov(mu[i],sigma[i])

def draw_mean_cov(mu,sigma):

    def sortedEigenValuesAndVectors(sigma):

        eigenValues, eigenVectors = LA.eigh(sigma)
        sorted_indices = np.argsort(eigenValues) #Returns the indices that would sort an array.
        return eigenValues[sorted_indices], eigenVectors[:, sorted_indices]


    ax = plab.gca() #get current axis

    eigenValues, eigenVectors = sortedEigenValuesAndVectors(sigma)
    theta = np.degrees(np.arctan2(*eigenVectors[:, 0][::-1]))


    width,height = 3 * np.sqrt(abs(eigenValues))
    ellip = Ellipse(xy=mu, width=width, height=height, angle=theta,color='r')

    ax.add_artist(ellip)
    return ellip


def initialize_2D(classes,samples):
    w, h = classes, samples
    result = [[0 for x in range(w)] for y in range(h)]
    return result

def data_generation(clusters):

    mean = [[-4, 0],[10, 12],[-10, 12],[0,20],[-15,20],[15,25],[-15,-10],[15,-10],[0,-10]]
    cov = [[[6, 0], [0, 2]], [[5, 0], [0, 20]], [[20, 2], [2, 5]],[[6, 0], [0, 2]],[[2, 0], [0, 6]],[[6, 0], [0, 15]],[[2, 0], [0, 6]],[[2, 0], [0, 6]],[[6, 0], [0, 2]]]

    x= []
    y= []

    mx = -5
    my = -5

    for i in range (clusters):
        r = np.random.randint(0,7)
        if (clusters<10):
            a, b = np.random.multivariate_normal(mean[i], cov[i], 500).T
        else: a, b = np.random.multivariate_normal([mx,my], cov[r], 500).T
        x = np.array(list(x) + list(a))
        y = np.array(list(y) + list(b))
        mx+=8
        my+=8



    combined = np.vstack((x, y)).T
    return combined

def initializeParameters(points,clusters):

    points = np.array(points)
    a = np.random.randint(0,points.shape[0],clusters)
    mu = initialize_2D(2,clusters)
    for i in range(clusters):
        mu[i][0] = points[a[i]][0]
        mu[i][1] = points[a[i]][1]

    sigma = [np.eye(2)]* clusters
    w = np.random.randint(1,1000,clusters);
    sum = 0
    for i in range(clusters):
        sum +=w[i]
    w = w/sum

    mu = np.array(mu)
    sigma = np.array(sigma)
    return mu,sigma,w;


def Pdfs(points,mu,sigma,clusters):
    y = initialize_2D(clusters, points.shape[0])
    for i in range(clusters):
        y[i] = multivariate_normal.pdf(points, mu[i], sigma[i]);
    return y

def E_Step(n_matrix,w,points,clusters):

    pij = np.zeros((points.shape[0], clusters))
    for i in range(clusters):
        pij[:,i] = w[i]*n_matrix[i]
    pij = (pij.T / np.sum(pij, axis=1)).T # running horizontally across columns (axis 1).


    return pij;

def M_Step(pij,mu,sigma,points,clusters):
    points_per_distribution = np.sum(pij, axis = 0) #running vertically downwards across rows (axis 0),
    point_minus_mu = np.zeros((clusters,points.shape[0], 2))

    for i in range(clusters):
        sum_x = 0
        sum_y = 0
        for j in range (points.shape[0]):
            sum_x= sum_x+ pij[j][i]*points[j][0]
            sum_y = sum_y + pij[j][i] * points[j][1]

        point_minus_mu[i] = points - mu[i]
        mu[i][0] = sum_x/points_per_distribution[i]
        mu[i][1] = sum_y/points_per_distribution[i]


    for i in range(clusters):
        sigma[i] = np.dot(np.multiply(point_minus_mu[i].T,pij[:,i]),point_minus_mu[i])
        sigma[i] = sigma[i]/points_per_distribution[i]

    w = points_per_distribution/points.shape[0]

    return mu,sigma,w;


def logLikelihood(n_matrix,w,points,clusters):
    temp = np.zeros((points.shape[0], clusters))
    for i in range(clusters):
        temp[:, i] = w[i] * n_matrix[i]

    log_likelihood = np.sum(np.log(np.sum(temp, axis=1))) #running horizontally across columns (axis 1).
    return log_likelihood

def main():

    clusters = int(input("number of clusters?"))
    np.random.seed(30)
    points = data_generation(clusters)
    np.random.shuffle(points)

    pyplt.plot(points[:,0], points[:,1], 'x')
    pyplt.axis('equal')
    pyplt.show()

    mu,sigma,w=initializeParameters(points,clusters)
    print(mu)
    print(sigma)
    print(w)


    fig = plab.figure(figsize=(10, 6))
    plot(points, mu, sigma,clusters)
    plab.show()


    prev = -9999.99
    while(True):

        n_matrix = Pdfs(points,mu,sigma,clusters)

        pij=E_Step(n_matrix,w,points,clusters)

        mu,sigma,w = M_Step(pij,mu,sigma,points,clusters)

        log_L=logLikelihood(n_matrix,w,points,clusters)

        fig = plab.figure(figsize=(10, 6))
        plot(points, mu, sigma,clusters)
        plab.show()


        if(np.abs(log_L-prev)<0.001):
            break
        prev = log_L

    print(mu)
    print(sigma)
    print(w)
    fig = plab.figure(figsize=(10, 6))
    plot(points, mu, sigma, clusters)
    plab.show()





if __name__ == '__main__':
    main()