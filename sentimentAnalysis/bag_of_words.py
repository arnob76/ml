import nltk
nltk.download('stopwords')
nltk.download('punkt')
import re, math, collections
import nltk, nltk.classify.util, nltk.metrics
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import stopwords
import string

#accumulating stop words, punctuations and irrelevant words
punctuation = list(string.punctuation)
stop_words = stopwords.words('english') + punctuation + ['RT', 'via']

class Sentiment:

    name = ""
    file = None
    sentences = None
    features = []

    def __init__( self, name ):

        self.name = name
        self.features = []

    def read_inputs( self ):

        self.file = open(self.name + '.txt', 'r')

    def split_dataset_by_sentences( self ):

        self.sentences = re.split(r'\n', self.file.read())

    def split_sentences_by_words( self, feature_select ):

        for sentence in self.sentences:
            words = re.findall(r"[\w']+|[.,!?;]",sentence.rstrip())
            words = [word for word in words if not word in stop_words]
            words = [feature_select(words), self.name]
            self.features.append(words)

    def get_train_cutoff( self ):

        cutoff = int(math.floor(len(self.features) * 3 / 4))
        return cutoff

    def get_features( self ):
        return self.features


def get_sentiments( classes ):

    sentiments = []

    for i in range(5):
        sentiment = Sentiment(classes[i])
        sentiments.append(sentiment)

    return sentiments


def get_all_features( sentiments, feature_select ):

    trainFeatures = []
    testFeatures = []

    for i in range(5):

        sentiments[i].read_inputs()
        sentiments[i].split_dataset_by_sentences()
        sentiments[i].split_sentences_by_words(feature_select)

        features = sentiments[i].get_features()
        cutoff = sentiments[i].get_train_cutoff()

        trainFeatures+= features[:cutoff]
        testFeatures+= features[cutoff:]

    return trainFeatures,testFeatures

def get_accuracy( classifier, testFeatures ):

    referenceSets = collections.defaultdict(set)
    testSets = collections.defaultdict(set)

    for i, ( features, label ) in enumerate( testFeatures ):
        referenceSets[ label ].add(i)
        predicted = classifier.classify( features )
        testSets[ predicted ].add(i)

    accuracy = nltk.classify.util.accuracy( classifier, testFeatures )
    return  accuracy,classifier

def print_results( accuracy, trainFeatures, testFeatures, classifier ):

    print('train on %d instances, test on %d instances' % (len(trainFeatures), len(testFeatures)))
    print(accuracy)
    classifier.show_most_informative_features(10)

def evaluate_features( feature_select ):

    classes = ['opinions', 'deals', 'events', 'news', 'pms']
    sentiments = get_sentiments(classes)

    trainFeatures, testFeatures = get_all_features(sentiments,feature_select)
    classifier = NaiveBayesClassifier.train(trainFeatures)

    accuracy,classifier = get_accuracy(classifier,testFeatures)

    print_results(accuracy,trainFeatures,testFeatures,classifier)

def make_full_dictionary( words ):

    return dict([(word, True) for word in words])

def main():

    print('using all words as features')
    evaluate_features(make_full_dictionary)

if __name__ == '__main__':
    main()

