import json
import pandas as pd
import matplotlib.pyplot as plt
import re

def word_in_text( word , text ):

    word = word.lower()
    text = text.lower()
    match = re.search( word, text )
    if match:
        return True
    return False

def extract_link( text ):

    #returns link if there is any, otherwise returns an empty string

    regex = r'https?://[^\s<>"]+|www\.[^\s<>"]+'
    match = re.search( regex, text )
    if match:
        return match.group()
    return ''

def readFromFileToDataframe():

    tweets_data = []
    tweets_file = open( 'twitter_data.txt', "r" )
    for line in tweets_file:
        try:
            tweet = json.loads( line )
            tweets_data.append( tweet )
        except:
            continue

    tweets = pd.DataFrame.from_dict(tweets_data)

    return tweets

def getLanguageCounts( tweets ):

    tweets_by_lang = tweets['lang'].value_counts()
    return tweets_by_lang

def plotLanguageCounts( tweets_by_lang ):

    fig, ax = plt.subplots()
    ax.tick_params(axis='x', labelsize=15)
    ax.tick_params(axis='y', labelsize=10)
    ax.set_xlabel('Languages', fontsize=15)
    ax.set_ylabel('Number of tweets', fontsize=15)
    ax.set_title('Top 5 languages', fontsize=15, fontweight='bold')
    tweets_by_lang[:5].plot(ax=ax, kind='bar', color='red')
    plt.show()

def addFieldsToDataframe(tweets):

    #A tweet is 'relevant' if it contains keywords 'programming'/'tutorial'

    tweets['python'] = tweets['text'].apply( lambda tweet: word_in_text('python', tweet) )
    tweets['javascript'] = tweets['text'].apply( lambda tweet: word_in_text('javascript', tweet) )
    tweets['ruby'] = tweets['text'].apply( lambda tweet: word_in_text('ruby', tweet) )
    tweets['relevant'] = tweets['text'].apply( lambda tweet: word_in_text('programming', tweet) or word_in_text('tutorial', tweet) )
    tweets['link'] = tweets['text'].apply( lambda tweet: extract_link(tweet) )

    return tweets

def getRelevantTweetsWithLink( tweets ):

    tweets_relevant = tweets[tweets['relevant'] == True]
    tweets_relevant_with_link = tweets_relevant[tweets_relevant['link'] != '']

    return tweets_relevant_with_link

def printLinks( tweets_relevant_with_link ):

    print (tweets_relevant_with_link[tweets_relevant_with_link['python'] == True]['link'])
    print (tweets_relevant_with_link[tweets_relevant_with_link['javascript'] == True]['link'])
    print (tweets_relevant_with_link[tweets_relevant_with_link['ruby'] == True]['link'])

def main():

    tweets = readFromFileToDataframe()

    tweets_by_lang = getLanguageCounts( tweets )
    plotLanguageCounts( tweets_by_lang )

    tweets = addFieldsToDataframe( tweets )
    tweets_relevant_with_link = getRelevantTweetsWithLink( tweets )

    printLinks( tweets_relevant_with_link )

if __name__ == '__main__':
    main()
